# This is a proxy project

Please have a look at the referenced projects.

This repo is linked from https://www.hackster.io/richard42/dromes-mavimg-e0422f "drone messenger - mavlink images".


Implementing this project I met several obstacles:

- One problem for me was the delay in shipment => collided with other higher priority work.

- Another problem I experienced was linked to the radio hardware. I had a terrible error rate and searched for the problem in my programming first. At the end the image transmission software I wrote can also be used to analyse the error rate.


The time put into the project so far by far exceeds the first estimation and I learned way more then I expected to learn about mavlink, pymavlink, radios, ... .

## Image transmission via mavlink
See https://gitlab.com/rm/mavmit


## Image recognition with OpenCV
to be done
